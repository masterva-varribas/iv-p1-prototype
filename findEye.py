# Copyright (c) 2015
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# Domain: Master Vision Artificial, URJC
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>

import cv2

eye_cascade = cv2.CascadeClassifier('haar-c/haarcascade_eye_tree_eyeglasses.xml')
def findEye(imrgb):
    imgray = cv2.cvtColor(imrgb, cv2.COLOR_RGB2GRAY)
    cv2.equalizeHist( imgray, imgray );

    eyes = eye_cascade.detectMultiScale(imgray)
    if (len(eyes) == 0):
        print 'No eye detected'
        return None;
    else:
        return eyes[0]
    
    
    
def cropEye(imrgb):
    rect = findEye(imrgb)
    if (rect is None): return None
    (x,y,w,h) = rect
    eye = imrgb[y:y+h, x:x+h]
    return eye