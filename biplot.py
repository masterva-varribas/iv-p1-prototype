# coding: utf-8
# Copyright (c) 2015
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# Domain: Master Vision Artificial, URJC
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>

import matplotlib.pyplot as plt
import matplotlib.cm as cm
    
def biplot(left, right, cmap=cm.gray):
    plt.subplot(1,2,2), plt.imshow(left, cmap=cmap);
    plt.subplot(1,2,1), plt.imshow(right, cmap=cmap);
    plt.show();
