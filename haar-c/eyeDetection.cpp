#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <stdio.h>


using namespace std;
using namespace cv;

/** Function Headers */
void detectAndDisplay( Mat frame );

/** Global variables */
String eyes_cascade_name = "../haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier eyes_cascade;
String window_name = "Capture - Eye detection";


/** @function main */
int main( void )
{
    if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading eyes cascade\n"); return -1; };

    Mat frame;

    frame = imread("../../left.png");

    //imshow(window_name, frame);
    //waitKey(0);

    detectAndDisplay( frame );

    waitKey(0);
}


/** @function detectAndDisplay */
void detectAndDisplay( Mat frame )
{
    Mat frame_gray;

    cvtColor( frame, frame_gray, COLOR_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );


    std::vector<Rect> eyes;

    //-- Detect eyes
    eyes_cascade.detectMultiScale( frame_gray, eyes, 1.1, 2, 0|CASCADE_SCALE_IMAGE, Size(30, 30) );
    std::cout<<eyes.size()<<std::endl;

    for ( size_t j = 0; j < eyes.size(); j++ )
    {
        Point eye_center( eyes[j].x + eyes[j].width/2, eyes[j].y + eyes[j].height/2 );
        int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
        circle( frame, eye_center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );

        cv::Rect eye_roi(eyes[j].x, eyes[j].y, eyes[j].width, eyes[j].height);
        rectangle(frame, eye_roi, Scalar(0,0,255), 2, 8, 0);
    }

    //-- Show what you got
    imshow( window_name, frame );
}
