# coding: utf-8

# Copyright (c) 2015
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# Domain: Master Vision Artificial, URJC
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>


import matplotlib.pyplot as plt
import matplotlib.cm as cm
import cv2
import numpy as np
    
def biplot(left, right, cmap=cm.gray):
    plt.subplot(1,2,2), plt.imshow(left, cmap=cmap);
    plt.subplot(1,2,1), plt.imshow(right, cmap=cmap);
    plt.show();


def bgr2rgb(im):
    b,g,r = cv2.split(im)
    return cv2.merge(r,g,b)


def imreadRGB(name):
    im = cv2.imread(name)
    return cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

