# Copyright (c) 2015
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# Domain: Master Vision Artificial, URJC
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>


import cv2
import numpy as np

"""
Hough gradient (borders) debugging
"""
canny_param = 80
def canny(imgray):
    canny = cv2.Canny(imgray, canny_param,canny_param/2)
    return canny



def houghCircles(imgray, rmin, rmax):
    circles = cv2.HoughCircles(imgray,cv2.cv.CV_HOUGH_GRADIENT,
    0.5, 1,
    param1=canny_param, param2=20,
    minRadius=rmin, maxRadius=rmax
    )

    return circles


def draw_circles(imrgb, circles, color=(255,0,0)):
    result = imrgb.copy()
    if (circles is None):
        print 'No circles'
        return result

    circles = np.array(np.around(circles), np.uint16)
    for i in circles[0,:]:
        cv2.circle(result,(i[0],i[1]),i[2],color,1)

    return result


#----

def mean_circle(circles):
    if (circles is None):
        return []

    circle = np.zeros((3,1), np.float)
    circles = np.array(np.around(circles), np.uint16)
    N = len(circles[0,:])
    for c in circles[0,:]:
        circle[0] += c[0]
        circle[1] += c[1]
        circle[2] += c[2]
    circle = circle/N
    
    return circle.astype(np.uint16)



def circ_mask(im, c):
    mask = np.zeros(im.shape, np.uint8)
    cv2.circle(mask,(c[0],c[1]),c[2],(1,1,1),-1)
    return mask


#----


iris_max_ratio = 1/4.0
iris_min_ratio = 1/8.0

def houghIris(imrgb):
    imgray = toSmoothGray(imrgb)
    (rows, cols) = imgray.shape
    
    rmin = int(1+cols*iris_min_ratio)
    rmax = int(1+cols*iris_max_ratio)
    #print rmin,rmax
    
    circles = houghCircles(imgray, rmin, rmax)
    c = mean_circle(circles)
    
    return c