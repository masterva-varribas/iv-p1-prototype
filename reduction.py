# coding: utf-8

# Copyright (c) 2015
# Author: Victor Arribas <v.arribas.urjc@gmail.com>
# Domain: Master Vision Artificial, URJC
# License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>


import cv2
import numpy as np

def colorRedution(im, rc=64):
    lookUpTable = np.zeros((1,256), 'uint8')
    for i in range(0,256):
        lookUpTable[0,i] = i/rc * rc
    
    im = cv2.LUT(im, lookUpTable)
    return im


def blurReduction(im, km=5, kg=3, nit=3):
    for i in range(0,nit):
        im = cv2.medianBlur(im, km)
        im = cv2.GaussianBlur(im, (kg,kg), 1)
        return im
    
